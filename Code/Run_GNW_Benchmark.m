close all hidden; close all; clear all; clc
addpath(genpath('~/GS'))

%% Data generation tool
tool = {'GeneNetWeaver', 'GeneSPIDER'};

%% Noise level
level = {'High', 'Medium', 'Low'};

for s = 1:length(tool)
  for l = 1:length(level)
    %% load networks and datasets
    load(strcat('~/BenchmarkBitbucket/', tool{s}, '_', level{l}, 'Noise.mat'))

    for i = 1:length(Data) %% There are 5 datasets and networks in each of Data and Net variables

      %% make sure all self loops exst in true GRN for the P-based methods least squares and z-score
      %% Glmnet methods run separately with a different sparsity handling
      network = Net(i).A;
      network(find(eye(Data(i).N)))=1;

      % Ordinary least squares
      A_ls(:,:,i) = -Data(i).P*pinv(Data(i).Y);

      % Calculate accuracy
      M_ls(i) = analyse.per_edge_correctness_count(network, A_ls(:,:,i));
      % to make sure the PR curve reaches the edges for the full area
      Mcoms = [1 M_ls(i).comspe 0]; Mpre = [0 M_ls(i).pre M_ls(i).pre(length(M_ls(i).pre))]; Msen = [1 M_ls(i).sen 0];
      AUPR_ls(i) = -trapz(Msen, Mpre); AUROC_ls(i) = -trapz(Mcoms, Msen);
      AUPR_ls(i)

      [~,b] = max(M_ls(i).F1);
      [~,a] = max(M_ls(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 1, Data(i).SNR_L, AUROC_ls(i), AUPR_ls(i), M_ls(i).F1(b), M_ls(i).nlinks(b), M_ls(i).MCC(a), M_ls(i).nlinks(a)))', '-append');

      %% Z-score
      Zsc = zeros(Data(i).N,Data(i).N);
      for z = 1:size(Data(i).Y,1)
        loc = find(Data(i).P(z,:));
        d = Data(i).Y(:,loc); ave = mean(d,2);
        for w = 1:length(ave)
          Zsc(w,z) = (ave(w) - mean(Data(i).Y(w,:)))/std(Data(i).Y(w,:));
        end
      end
      A_zscore(:,:,i) = Zsc;

      M_zscore(i) = analyse.per_edge_correctness_count(network, A_zscore(:,:,i));
      Mcoms = [1 M_zscore(i).comspe 0]; Mpre = [0 M_zscore(i).pre M_zscore(i).pre(length(M_zscore(i).pre))]; Msen = [1 M_zscore(i).sen 0];
      AUPR_Zscore(i) = -trapz(Msen, Mpre); AUROC_Zscore(i) = -trapz(Mcoms, Msen);
      AUPR_Zscore(i)

      [~,b] = max(M_zscore(i).F1);
      [~,a] = max(M_zscore(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 2, Data(i).SNR_L, AUROC_Zscore(i), AUPR_Zscore(i), M_zscore(i).F1(b), M_zscore(i).nlinks(b), M_zscore(i).MCC(a), M_zscore(i).nlinks(a)))','-append');


      %% make sure all self loops are removed from true GRN for the non P-based methods
      network = Net(i).A;
      network(find(eye(Data(i).N)))=0;

      %% GENIE3
      A_genie3(:,:,i) = genie3(Data(i).Y'); % Data(i).Y has genes in rows and experiments in columns but Genie3 requires the opposite so the data is transposed.

      %% Genie3 is a special case because the documented format states that the output GRN has regulators in rows and targets in columns.
      %% However, we obtain higher accuracy with the opposite format where we consider columns as regulators and rows targets (which is the same as the true GRN format here)
      %% Therefore, even though in a normal situation we should have transposed the Genie3 output to be comparable to the true GRN's format, we do not to achieve higher accuracy.
      M_genie3(i) = analyse.per_edge_correctness_count(network, A_genie3(:,:,i));

      % making sure the inferred GRNs reach the edges for full PR area
      Mcoms = [1 M_genie3(i).comspe 0]; Mpre = [0 M_genie3(i).pre M_genie3(i).pre(length(M_genie3(i).pre))]; Msen = [1 M_genie3(i).sen 0];
      AUPR_genie3(i) = -trapz(Msen, Mpre); AUROC_genie3(i) = -trapz(Mcoms, Msen);
      AUPR_genie3(i)

      [~,b] = max(M_genie3(i).F1);
      [~,a] = max(M_genie3(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 3, Data(i).SNR_L, AUROC_genie3(i), AUPR_genie3(i), M_genie3(i).F1(b), M_genie3(i).nlinks(b), M_genie3(i).MCC(a), M_genie3(i).nlinks(a)))','-append');


      %% PLSNET
      pn = plsnet(Data(i).Y'); % Data(i).Y has genes in rows and experiments in columns but Plsnet requires the opposite so the data is transposed.
      A_plsnet(:,:,i) = pn'; % transposing the inferred GRN because the output is from-row-to-column but the true GRN is from-column-to-row

      M_plsnet(i) = analyse.per_edge_correctness_count(network, A_plsnet(:,:,i));
      Mcoms = [1 M_plsnet(i).comspe 0]; Mpre = [0 M_plsnet(i).pre M_plsnet(i).pre(length(M_plsnet(i).pre))]; Msen = [1 M_plsnet(i).sen 0];
      AUPR_plsnet(i) = -trapz(Msen, Mpre); AUROC_plsnet(i) = -trapz(Mcoms, Msen);
      AUPR_plsnet(i)

      [~,b] = max(M_plsnet(i).F1);
      [~,a] = max(M_plsnet(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 4, Data(i).SNR_L, AUROC_plsnet(i), AUPR_plsnet(i), M_plsnet(i).F1(b), M_plsnet(i).nlinks(b), M_plsnet(i).MCC(a), M_plsnet(i).nlinks(a)))','-append');


      %% CLR
      A_clr(:,:,i) = clr(Data(i).Y, 'rayleigh', 10, 3);

      M_clr(i) = analyse.per_edge_correctness_count(network, A_clr(:,:,i));
      Mcoms = [1 M_clr(i).comspe 0]; Mpre = [0 M_clr(i).pre M_clr(i).pre(length(M_clr(i).pre))]; Msen = [1 M_clr(i).sen 0];
      AUPR_clr(i) = -trapz(Msen, Mpre); AUROC_clr(i) = -trapz(Mcoms, Msen);
      AUPR_clr(i)

      [~,b] = max(M_clr(i).F1);
      [~,a] = max(M_clr(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 5, Data(i).SNR_L, AUROC_clr(i), AUPR_clr(i), M_clr(i).F1(b), M_clr(i).nlinks(b), M_clr(i).MCC(a), M_clr(i).nlinks(a)))','-append');


      %% TIGRESS
      DataT = struct('expdata', Data(i).Y');
      tfindices = double(1:size(Data(i).Y,1));
      freq = tigress(DataT,'R', 1000, 'alpha', 0.4, 'L',2);
      scores = score_edges(freq);
      edges = predict_network(scores, tfindices);
      A_tig = zeros(size(Data(i).Y,1), size(Data(i).Y,1));
      for j = 1:size(edges,1)
        A_tig(edges(j,2), edges(j,1)) = edges(j,3);
      end
      A_tigress(:,:,i) = A_tig;

      M_tigress(i) = analyse.per_edge_correctness_count(network, A_tigress(:,:,i));
      Mcoms = [1 M_tigress(i).comspe 0]; Mpre = [0 M_tigress(i).pre M_tigress(i).pre(length(M_tigress(i).pre))]; Msen = [1 M_tigress(i).sen 0];
      AUPR_tigress(i) = -trapz(Msen, Mpre); AUROC_tigress(i) = -trapz(Mcoms, Msen);
      AUPR_tigress(i)

      [~,b] = max(M_tigress(i).F1);
      [~,a] = max(M_tigress(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/All','.csv']),(vertcat(s, i, Data(i).N, l, 6, Data(i).SNR_L, AUROC_tigress(i), AUPR_tigress(i), M_tigress(i).F1(b), M_tigress(i).nlinks(b), M_tigress(i).MCC(a), M_tigress(i).nlinks(a)))','-append');

      %% LASSO, ElasticNet, RidgeCO and BC3NET will run separately
    end
    save(fullfile(['/cfs/klemming/scratch/s/secilmis/PerEdge_', tool{s}, '_', level{l}, 'Noise.mat']), 'A_ls', 'M_ls', 'A_zscore', 'M_zscore', 'A_genie3', 'M_genie3', 'A_plsnet', 'M_plsnet', 'A_clr', 'M_clr', 'A_tigress', 'M_tigress')
    clear net A_ls M_ls AUROC_ls AUPR_ls A_zscore M_zscore AUROC_Zscore AUPR_Zscore A_genie3 M_genie3 AUROC_genie3 AUPR_genie3 A_plsnet M_plsnet AUROC_plsnet AUPR_plsnet A_clr M_clr AUROC_clr AUPR_clr A_tigress M_tigress AUROC_tigress AUPR_tigress
  end
end


%%% Now running Glmnet-based methods

close all hidden; close all; clear all; clc
addpath(genpath('~/GS'))


%% Data generation tool
tool = {'GeneNetWeaver', 'GeneSPIDER'};

%% Noise level
level = {'High', 'Medium', 'Low'};

for s = 1:length(tool)
  for l = 1:length(level)
    %% load networks and datasets
    load(strcat('~/BenchmarkBitbucket/', tool{s}, '_', level{l}, 'Noise.mat'))

    for i = 1:length(Data)
      network = Net(i).A;
      network(find(eye(Data(i).N)))=1;

      A_lasso{i} = Methods.Glmnet(Data(i), logspace(-6,0,30));

      M_lasso(i) = analyse.calc_correctness_curves(network, A_lasso{i});
      Mcoms = [1 M_lasso(i).comspe 0]; Mpre = [0 M_lasso(i).pre M_lasso(i).pre(length(M_lasso(i).pre))]; Msen = [1 M_lasso(i).sen 0];
      AUPR_lasso(i) = -trapz(Msen, Mpre); AUROC_lasso(i) = -trapz(Mcoms, Msen);
      AUPR_lasso(i)

      [~,b] = max(M_lasso(i).F1);
      [~,a] = max(M_lasso(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/Glmnet','.csv']),(vertcat(s, i, Data(i).N, l, 1, Data(i).SNR_L, AUROC_lasso(i), AUPR_lasso(i), M_lasso(i).F1(b), M_lasso(i).nlinks(b), M_lasso(i).MCC(a), M_lasso(i).nlinks(a)))', '-append');


      A_el{i} = Methods.elasticNet(Data(i), logspace(-6,0,30));

      M_el(i) = analyse.calc_correctness_curves(network, A_el{i});
      Mcoms = [1 M_el(i).comspe 0]; Mpre = [0 M_el(i).pre M_el(i).pre(length(M_el(i).pre))]; Msen = [1 M_el(i).sen 0];
      AUPR_el(i) = -trapz(Msen, Mpre); AUROC_el(i) = -trapz(Mcoms, Msen);
      AUPR_el(i)

      [~,b] = max(M_el(i).F1);
      [~,a] = max(M_el(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/Glmnet','.csv']),(vertcat(s, i, Data(i).N, l, 2, Data(i).SNR_L, AUROC_el(i), AUPR_el(i), M_el(i).F1(b), M_el(i).nlinks(b), M_el(i).MCC(a), M_el(i).nlinks(a)))', '-append');

      A_ridge{i} = Methods.ridgeco(Data(i), logspace(-6,0,30));

      M_ridge(i) = analyse.calc_correctness_curves(network, A_ridge{i});
      Mcoms = [1 M_ridge(i).comspe 0]; Mpre = [0 M_ridge(i).pre M_ridge(i).pre(length(M_ridge(i).pre))]; Msen = [1 M_ridge(i).sen 0];
      AUPR_ridge(i) = -trapz(Msen, Mpre); AUROC_ridge(i) = -trapz(Mcoms, Msen);
      AUPR_ridge(i)

      [~,b] = max(M_ridge(i).F1);
      [~,a] = max(M_ridge(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/Glmnet','.csv']),(vertcat(s, i, Data(i).N, l, 3, Data(i).SNR_L, AUROC_ridge(i), AUPR_ridge(i), M_ridge(i).F1(b), M_ridge(i).nlinks(b), M_ridge(i).MCC(a), M_ridge(i).nlinks(a)))', '-append');
    end
    save(fullfile(['/cfs/klemming/scratch/s/secilmis/Glmnet_', tool{s}, '_', level{l}, 'Noise.mat']), 'A_lasso', 'M_lasso', 'A_el', 'M_el', 'A_ridge', 'M_ridge')
    clear network  A_lasso M_lasso AUROC_lasso AUPR_lasso A_el M_el AUROC_el AUPR_el A_ridge M_ridge AUROC_ridge AUPR_ridge
  end
end


%% Now running BC3NET

close all hidden; close all; clear all; clc
addpath(genpath('~/GS'))

%% Data generation tool
tool = {'GeneNetWeaver', 'GeneSPIDER'};

%% Noise level
level = {'High', 'Medium', 'Low'};

for s = 1:length(tool)
  for l = 1:length(level)
    %% load networks and datasets
    load(strcat('~/BenchmarkBitbucket/', tool{s}, '_', level{l}, 'Noise.mat'))

    for i = 1:length(Data)
      %% Assign zeros to selfloops in true net
      network = Net(i).A;
      network(find(eye(Data(i).N)))=0;

      %% BC3NET
      A_bc3net(:,:,i) = Methods.BC3NET(Data(i));

      M_bc3net(i) = analyse.per_edge_correctness_count(network, A_bc3net(:,:,i));
      Mcoms = [1 M_bc3net(i).comspe 0]; Mpre = [0 M_bc3net(i).pre M_bc3net(i).pre(length(M_bc3net(i).pre))]; Msen = [1 M_bc3net(i).sen 0];
      AUPR_bc3net(i) = -trapz(Msen, Mpre); AUROC_bc3net(i) = -trapz(Mcoms, Msen);
      AUPR_bc3net(i)

      [~,b] = max(M_bc3net(i).F1);
      [~,a] = max(M_bc3net(i).MCC);
      dlmwrite(fullfile(['/cfs/klemming/scratch/s/secilmis/BC3NET','.csv']),(vertcat(i, Data(i).N, l, 7, Data(i).SNR_L, AUROC_bc3net(i), AUPR_bc3net(i), M_bc3net(i).F1(b), M_bc3net(i).nlinks(b), M_bc3net(i).MCC(a), M_bc3net(i).nlinks(a)))','-append');
      end
      save(fullfile(['/cfs/klemming/scratch/s/secilmis/PerEdge_BC3NET_', tool{s}, '_', level{l}, 'Noise.mat']), 'A_bc3net', 'M_bc3net')
      clear A_bc3net M_bc3net t_bc3net
  end
end
