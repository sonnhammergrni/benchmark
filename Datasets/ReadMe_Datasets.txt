This folder contains six .mat files: three from GeneNetWeaver, and three from GeneSPIDER.
Each data generation tool has three different noise levels: 'High', 'Medium', and 'Low'.
- True GRNs and noise-free gene expression datasets are generated using either GeneNetWeaver or GeneSPIDER
- Gaussian noise based on three fixed signal-to-noise ratio levels are calculated for each noise-free gene expression
- Noise-free gene expression + Noise are stored in a GeneSPIDER data structure.
Each .mat file contains five true GRNs and five datasets (GeneSPIDER datastructures).
Each true GRN and noise-free gene expression for both GeneNetWeaver and GeneSPIDER are the same at all noise levels.
	- The only difference is the amount of noise added.
In order to be able to load the GeneSPIDER datastructures properly in Matlab, one should download the whole GeneSPIDER repo first, and make sure its path is added.
