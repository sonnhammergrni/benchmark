# Benchmarking the effect of using perturbation design in GRN inference

Repository for the paper: "Knowledge of the perturbation design is essential for accurate gene regulatory network inference"
<!-- add paper link here -->

![workflow](https://bitbucket.org/sonnhammergrni/benchmark/raw/7785595fbfc14fb2b4f0781dc5d5bd31afd336d1/Untitled-3.png)

This repository contains all code and data needed to run the benchmark as described in the paper. It further contains all results that were used in the paper.

