This folder contains 3 .csv files for 
(1) the main benchmark results shown in Figs.2-3 in the main article file. Column heading corresponds to 
	- Tool: data generation tool used to generate the in silico data, whether GeneNetWeaver or GeneSPIDER
	- Dataset: says which network in the .mat file it corresponds to. There are 5 different datasets generated from 5 true GRNs
	- NrGene: number of genes in the dataset. It is 100 for all datasets in this benchmark
	- NoiseLevel: 'High', 'Medium' or 'Low'. 
		- For example, Dataset 1 with high noise and Dataset 2 with high noise both come from the same true GRN and noise-free gene expression data, 
		but just the amount of noise added is different.
	- Method: GRN inference method. 'Least Squares', 'LASSO', 'ElasticNet', 'Ridge Regression', 'Z-score', 'GENIE3', 'PLSNET', 'CLR', 'TIGRESS', 'BC3NET'
	- SNR_L: signal-to-noise ratio calculated based on Eq. 1 in the Online Methods section of the main article file
	- AUROC: area under receiver-operating-characteristic
	- AUPR: area under precision-recall
	- MaxF1: maximum F1-score within the entire set of GRNs at different sparsities
	- F1_nrlink: number of links in GRN which gives the maximum F1-score. In order to obtain the number of links per node, one should divide F1_nrlink by NrGene
	- MaxMCC: maximum Matthew's correlation coefficient (MCC) within the entire set of GRNs at different sparsities
	- MCC_nrlink: number of links in GRN which gives the maximum MCC. In order to obtain the number of links per node, one should divide F1_nrlink by NrGene
	- Ptype: refers to the perturbation design backgroung of the GRN inference method ('Method'), whether 
		- 'P-based' (if they utilize the perturbation design), 
		- 'NonP-based' (if they do not utilize the perturbation design), or 
		- 'Shuffled P-based' (if they are forced to utilize the incorrect perturbation design)
